const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ObjectId = Schema.ObjectId;
const userSchema = new Schema({
    name: { type: String, default: null },
    email: { type: String, default: null },
    password: { type: String, default: null },
    //phone:{type:Number,default:null},
    // user_status:{type:String,default:'offline'},
    // created_at: { type: Date, default: Date.now() },
},{timestamps:true});
module.exports = mongoose.model("registeration", userSchema);