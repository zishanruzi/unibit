const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const sampleticketSchema = new Schema(
  {
    ticketName: String,
    ticketData: [[Number]]
  },
  { timestamps: true }
);

module.exports = mongoose.model("sampleticket", sampleticketSchema);
