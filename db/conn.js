const mongoose = require('mongoose');
mongoose.set("strictQuery", true);
mongoose.connect("mongodb://localhost:27017/UNIBIT")
  .then(() => {
    console.log("MongoDB connected successfully");
  })
  .catch((error) => {
    console.error("Failed to connect to MongoDB:", error);
  });