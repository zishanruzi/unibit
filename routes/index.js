var express = require('express');
var router = express.Router();

const register = require("../Controller/user");

const addticket = require("../Controller/sampleData");

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/register', register.userRegister);

router.post('/login', register.userLogin);

router.post('/add', addticket.addticekt);

router.get('/tambolastart', register.generateticket);



module.exports = router;

