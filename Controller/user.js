const user = require('../model/userRegister');
const sticket =require("../model/Sampledata");

const bcrypt = require('bcryptjs');

var jwt = require('jsonwebtoken');
const secret='12345';

/// jwt check

let userVerifytoken = (req, res) => {
  let token = req.headers;
  if (token) {
    //(`token1-> ${token}`);
    // token = token.split(" ")[1];
    //(`token2-> ${token}`);

    jwt.verify(token,secret, async (err, valid) => {
      //(token);
      if (err) {
        res.send({ status: 702 });
        // resp.status(200).send({message:"Please provide a valid token",errorCode:"700"});
      } else {
        const isExist = await user.findOne({ _id: valid.data });
        //(valid);
        if (isExist) {
          res.send({ isExist, status: 200 });
        } else {
          res.send({ status: 400 });
          // resp.status(200).send({message:"Please provide a valid token",errorCode:"701"});
        }
      }
    });
  } else {
    res.send({ message: "Please provide token", status: 800 });
  }
};





exports.userRegister=async(req,res)=> {
    try {
        let data= req.body;
console.log("body data", data );

const isExist = await user.findOne({
    email: data.email
});

if (isExist) {
    res.send({
      message: "user already registered" , code: 202
    })
} else {
    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(data.password, salt, async function (err, hash) {
          // Store hash in your password DB.
          if (err) {
            console.log(err);
          } else {
            console.log(hash);
            let objCreate ={
              ...data,
              password:hash
            }
            // const objCreate = {
            //   name: name,
            //   email: email,
            //   phone:phone,
            //   // user_status:'offline',
            //   password: hash
            // };
            const createData = await user.create(objCreate);
            if(createData){
              res.send({ status:200,  message: "Registration successfull" });
            }else{
              res.send({ status:400,  message: "Registration failed" });
            }
          }
        })
  
      });
}

    } catch (error) {
        res.send({ status:404,  message: "something went wrong" });  
    }
}


//log in user

exports.userLogin=async(req,res) => {
    try {
        let data = req.body;
  console.log("data ", data.email);
  const isExist = await user.findOne({ email: data.email });
  if (isExist) {
    bcrypt.compare(data.password, isExist.password, async function (err, result) {
        // result == true
        if (result) {
          // const updateResponse = await user.updateOne({ email: email }, { $set: { user_status: 'online' } });
          const token = jwt.sign({
            data: isExist._id
          }, secret,
          {
            expiresIn: "365d", // expires in 365 days
          });
          console.log("token-->", isExist);
          let val = {
            name:isExist.name,
            email:isExist.email
          }
        
          res.send({status:200 , message: 'Logged in Successfully',data:val, token: token })
        } else {
          res.send({ status:400 ,message: 'Password do not match!' });
        }
      });
  } else {
    res.send({status:202, message: 'user not found' });
  }
    } catch (error) {
        console.log("something went wrong");
    }
}



function generateTicketNumbers(ticket) {
  const ticketData = ticket.ticketData;
  const ticketNumbers = [];

  // Generate numbers for each column
  for (let col = 0; col < 9; col++) {
    const columnNumbers = [];

    // Generate numbers for each row in the column
    for (let row = 0; row < 3; row++) {
      let number;

      // Find the next available number in the range of the column
      for (let i = col * 10 + 1; i <= col * 10 + 10; i++) {
        if (!ticketNumbers.includes(i)) {
          number = i;
          break;
        }
      }

      columnNumbers.push(number || "x");
      ticketNumbers.push(number);
    }

    ticketData[row][col] = columnNumbers;
  }

  return ticketData;
}



exports.generateticket = async (req, res, next) => {
  // let data = req.body;
  let token = req.headers["authorization"];
console.log("token",token);
  userVerifytoken(
    { headers: token },
    {
      send: async (e) => {
        switch (e.status) {
          case 200:
            const findUserProfile = await user.findOne({ _id: e.isExist._id });
            if (findUserProfile) {
              const ticketData = await sticket.find(); // Retrieve all ticket data
              console.log("ticket=============>",ticketData);
              if (ticketData.length > 0) {
                const randomIndex = Math.floor(Math.random() * ticketData.length);
                const ticket = ticketData[randomIndex];
                const ticketName = ticket.ticketName;
  const ticketNumbers = ticket.ticketData;

  console.log("Ticket Name:", ticketName);
  console.log("Ticket Numbers:", ticketNumbers);

                if (ticketName) {
                  const generatedTicketNumbers = generateTicketNumbers(ticketData, ticketName);
              console.log("generatedTicketNumbers Numbers:", generatedTicketNumbers);

                  res.send({ message: generatedTicketNumbers, statusCode: 200})
                  // Use the generatedTicketNumbers as needed
                } else {
                  res.send({ message: "Failed to generate ticket name", statusCode: 500 });
                }
              } else {
                res.send({ message: "Ticket data not found", statusCode: 404 });
              }
            } else {
              res.send({ message: "User not found", status: 401 });
            }
            break;
          case 400:
            res.send({ message: "Token expired", statusCode: 400 });
            break;
          case 800:
            res.send({ message: "Token expired", statusCode: 800 });
            break;
          case 702:
            res.send({ message: "Authorization error", statusCode: 702 });
            break;
          default:
            break;
        }
      },
    }
  );
};

function generateTicketNumbers() {
  const ticketNumbers = [];

  const columns = 9; // Number of columns in the ticket
  const rows = 3; // Number of rows in the ticket

  const numbersPerColumn = 10; // Number of numbers per column
  const numbersPerRow = 5; // Number of numbers per row

  const totalNumbers = columns * numbersPerColumn; // Total number of unique numbers

  // Generate an array with numbers 1 to 90
  const allNumbers = Array.from({ length: totalNumbers }, (_, index) => index + 1);

  // Shuffle the array randomly
  const shuffledNumbers = allNumbers.sort(() => 0.5 - Math.random());

  // Assign numbers to the ticket rows and columns
  for (let column = 0; column < columns; column++) {
    const ticketColumn = [];

    for (let row = 0; row < rows; row++) {
      const ticketRow = [];

      for (let i = 0; i < numbersPerRow; i++) {
        const index = column * numbersPerColumn + row + i * columns;
        const number = shuffledNumbers[index] || 0; // Use 0 for blank cells

        ticketRow.push(number);
      }

      ticketColumn.push(ticketRow);
    }

    ticketNumbers.push(ticketColumn);
  }

  return ticketNumbers;
}

// function getRandomTicketName(ticketData) {
//   if (ticketData && ticketData.length > 0) {
//     const randomIndex = Math.floor(Math.random() * ticketData.length);
//     return ticketData[randomIndex].ticketName;
//   } else {
//     return null;
//   }
// }